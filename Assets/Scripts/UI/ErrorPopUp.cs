using TMPro;
using UnityEngine;

namespace UI
{
   public class ErrorPopUp : MonoBehaviour
   {
      [SerializeField] private TextMeshProUGUI textArea;
      private void Awake()=>gameObject.SetActive(false);
      public void Close()=>gameObject.SetActive(false);
      public void SetText(string _text) => textArea.text = _text;
      
   }
}
