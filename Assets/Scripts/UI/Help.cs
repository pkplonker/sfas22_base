using TMPro;
using UnityEngine;

namespace UI
{
	public class Help : MonoBehaviour
	{
		[SerializeField] private UI ui;
		[TextArea] [SerializeField] private string textChars;
		[SerializeField] private PauseState pauseState;
		[SerializeField] private PauseMenu pauseMenu;
		private TextMeshProUGUI textMeshProObject;

		private void Awake()
		{
			textMeshProObject = GetComponentInChildren<TextMeshProUGUI>();
			textMeshProObject.text = textChars;
			var group = GetComponent<CanvasGroup>();
			group.interactable = false;
			group.blocksRaycasts = false;
			group.alpha = 0.0f;
		}

		public void Back()
		{
			ui.HideHelp();
			ui.ShowAuxButtons();
			if (pauseState.isPaused) pauseMenu.ShowMenu();
			else pauseMenu.HideMenu();
			
		}
	}
}