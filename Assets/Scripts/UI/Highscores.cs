using System.Collections.Generic;
using SaveSystem;
using SO;
using TMPro;
using UnityEngine;

namespace UI
{
   public class Highscores : MonoBehaviour
   {
      [SerializeField] private ScoreSO scores;
      [SerializeField] private UI ui;
      [SerializeField] private GameObject prefab;
      [SerializeField] private Transform container;
      [SerializeField] private int highScoresToDisplay = 5;
      [SerializeField] private ScoreSO.Levels currentFilterLevel = 0;
      [SerializeField] private TMP_Dropdown dropDown;
      [SerializeField] private GameObject confirmationPopUp;
      private SaveSystem.SaveSystem SaveSystem;

      //references and reset/reload from file
      private void Awake()
      {
         confirmationPopUp.SetActive(false);
         SaveSystem = global::SaveSystem.SaveSystem.instance;
         LoadScoresFromFile();
      }
      private void OnEnable()=>global::SaveSystem.SaveSystem.OnDataSaved += Refresh;
      private void OnDisable()=>global::SaveSystem.SaveSystem.OnDataSaved -= Refresh;
      

      //clear on initial load and reload from file
      void LoadScoresFromFile()
      {
         SaveData data = LoadSystem.Load();
         scores.LoadSaveData(data);
      }
      //back button
      public void Back()
      {
         ui.HideHighscoresMenu();
         ui.ShowMenu();
      }
      // reload data and display
      public void Refresh()
      {
         Clear();
         List<Score> sortedScores = scores.Sort();
         sortedScores = Filter(sortedScores, currentFilterLevel);
         int count = 0;
         //loads only 5 elements
         foreach (var score in sortedScores)
         {
            if (count >= highScoresToDisplay) break;
            count = GenerateHighScoreEntry(score, count);
         }
      }

      private int GenerateHighScoreEntry(Score score, int count)
      {
         GameObject newEntry = Instantiate(prefab, container);
         newEntry.GetComponent<HighScoresEntry>().Init(score);
         count++;
         return count;
      }

      //clears all old UI elements
      void Clear()
      {
         foreach (Transform child in container)
         {
            Destroy(child.gameObject);
         }
      }
   
      //return list containing only elements with correct level. Returns all if null.
      public static List<Score> Filter(List<Score> unfiltered, ScoreSO.Levels _currentFilterLevel)
      {
         List<Score> filteredScores = new List<Score>(unfiltered.Count);
         if (_currentFilterLevel == 0) return unfiltered;
         for (int i = 0; i < unfiltered.Count; i++)
         {
            if(unfiltered[i].GetLevel()==_currentFilterLevel) filteredScores.Add(unfiltered[i]);
         }
         return filteredScores;
      }
      //Funciton called from UI when dropdown changes. Sets level to dropdown value.
      public void ChangeLevel()
      {
         currentFilterLevel = (ScoreSO.Levels)dropDown.value ;
         if (currentFilterLevel < 0) currentFilterLevel = 0;
         Refresh();
      }
      //button to reset all scores
      public void ResetAllScores()=>confirmationPopUp.SetActive(true);
      
      //Confirms reset request, clears scores and saves to file
      public void ResetAllScoresConfirmed()
      {
         scores.Clear();
         Refresh();
         ResetAllScoresBack();
         SaveSystem.Save(new SaveData());
      }
   
      //back button to avoid resetting scores
      public void ResetAllScoresBack()=>confirmationPopUp.SetActive(false);
      
   
   }
}
