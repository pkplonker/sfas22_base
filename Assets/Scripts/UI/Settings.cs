using UnityEngine;

namespace UI
{
    //Manages Settings UI screen Functionality
    public class Settings : MonoBehaviour
    {
        [SerializeField] private UI ui;
        [SerializeField] private PauseState pauseState;
        [SerializeField] private PauseMenu pauseMenu;

        private void Awake()
        {
            var group = GetComponent<CanvasGroup>();
            group.interactable = false;
            group.blocksRaycasts = false;
            group.alpha = 0.0f;
        }
        
        public void Back()
        {
            ui.HideSettings();
            ui.ShowAuxButtons();
            if (pauseState.isPaused) pauseMenu.ShowMenu();
            else pauseMenu.HideMenu();
        }
    }
}
