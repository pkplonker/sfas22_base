using UnityEngine;

namespace UI
{
	public class AuxButtons : MonoBehaviour
	{
		[SerializeField] private UI ui;
		[SerializeField] private PauseState pauseState;
		[SerializeField] private PauseMenu pauseMenu;
		private void Awake() =>ui.ShowAuxButtons();
		

		public void Settings()
		{
			ui.ShowSettings();
			ui.HideAuxButtons();
			HandlePause();
		}

		public void Help()
		{
			ui.ShowHelp();
			ui.HideAuxButtons();
			HandlePause();
		}

		void HandlePause()
		{
			if (pauseState.isPaused) pauseMenu.ShowMenu();
			else pauseMenu.HideMenu();
		}
	}
}