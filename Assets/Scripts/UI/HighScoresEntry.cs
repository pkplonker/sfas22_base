using SO;
using TMPro;
using UnityEngine;

namespace UI
{
   public class HighScoresEntry : MonoBehaviour
   {
      [SerializeField] private TextMeshProUGUI levelText;
      [SerializeField] private TextMeshProUGUI difficultyText;
      [SerializeField] private TextMeshProUGUI time;

   
      //updates all text fields to passed in Data.
      public void Init(Score score)
      {
         levelText.text = score.GetLevel().ToString();
         int difficulty = (int)Mathf.Abs(score.GetDifficulty() * 100);
         difficultyText.text =difficulty.ToString();
         time.text = UI.FormatTime( score.GetTime());
      }

   
   }
}
