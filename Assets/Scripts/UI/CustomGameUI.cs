using System;
using SO;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
	public class CustomGameUI : MonoBehaviour
	{
		public event Action<SO.Level> OnCustomGame;

		[SerializeField] private TMP_InputField heightInput;
		[SerializeField] private TMP_InputField enemiesInput;
		[SerializeField] private UI ui;
		[FormerlySerializedAs("ErrorPopUp")] [SerializeField] private GameObject errorPopUp;
		[SerializeField] private string wrongSizeMessage = "Width & height must be between 4-22";
		[SerializeField] private string enemyMessage = "Enemies must be less than the number of tiles";

		//passes data required for game to create custom game
		public void StartButtonPress()
		{
			if (GetInput(out var size, out var enemies))
			{
				size *= size;
				OnCustomGame?.Invoke(new SO.Level((int) Mathf.Sqrt(size), (int) Mathf.Sqrt(size), enemies,
					ScoreSO.Levels.Custom));
			}
			else
			{
				if (size < 4||size > 22)
				{
					GeneratePopUp(wrongSizeMessage);
				}
				else if (enemies > size * size)
				{
					GeneratePopUp(enemyMessage);
				}

				heightInput.text = null;
				enemiesInput.text = null;
			}
		}

		private void GeneratePopUp(string displayText)
		{
			errorPopUp.SetActive(true);
			errorPopUp.GetComponent<ErrorPopUp>().SetText(displayText);
		}

		//func to validate data input
		private bool GetInput(out int size, out int enemies)
		{
			(size, enemies) = (0, 0);
			if (!Int32.TryParse(heightInput.text, out size)) return false;
			if (!Int32.TryParse(enemiesInput.text, out enemies)) return false;
			return (size >= 4 && size <= 22 && enemies < (size * size));
		}

		//ui back button to return to menu
		public void Back()
		{
			ui.HideCustomGameMenu();
			ui.ShowMenu();
		}
	}
}