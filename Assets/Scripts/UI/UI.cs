using System;
using System.Collections;
using SO;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UI : MonoBehaviour
    {
        [SerializeField] private CanvasGroup Menu;
        [SerializeField] private CanvasGroup Game;
        [SerializeField] private CanvasGroup Result;
        [SerializeField] private CanvasGroup CustomGame;
        [SerializeField] private CanvasGroup HighscoresMenu;
        [SerializeField] private CanvasGroup BackGround;
        [SerializeField] private CanvasGroup LevelSelect;
        [SerializeField] private CanvasGroup MiniMap;
        [SerializeField] private CanvasGroup AuxButtons;
        [SerializeField] private CanvasGroup Settings;
        [SerializeField] private CanvasGroup Help;
        [SerializeField] private TMP_Text TimerText;
        [SerializeField] private static CanvasGroup PauseMenu;
        private static readonly float AnimationTime = 1f;

         ResultsUI resultUI;
        //existing func

        public void ShowMenu()
        {
            StartCoroutine(ShowCanvas(Menu, 1.0f));
        }
        //existing func

        public void ShowGame()
        {
            StartCoroutine(ShowCanvas(Game, 1.0f));
        }
        //existing func modified for time output and formatting

        public void ShowResult(bool success, Score score)
        {
            resultUI.Show( success, score);
            StartCoroutine(ShowCanvas(Result, 1.0f));
        }
        //existing func

        public void HideMenu()
        {
            StartCoroutine(ShowCanvas(Menu, 0.0f));
        }
        //existing func

        public void HideGame()
        {
            StartCoroutine(ShowCanvas(Game, 0.0f));
        }
        //existing func

        public void HideResult()
        {
            StartCoroutine(ShowCanvas(Result, 0.0f));
        }
        //existing func
        public void UpdateTimer(double gameTime)
        {
            if (TimerText != null)
            {
                TimerText.text = FormatTime(gameTime);
            }
        }
        //existing func modified for custom game
        private void Awake()
        {
            resultUI = GetComponentInChildren<ResultsUI>();
            if (Menu != null)
            {
                Menu.alpha = 0.0f;
                Menu.interactable = false;
                Menu.blocksRaycasts = false;
            }

            if (Game != null)
            {
                Game.alpha = 0.0f;
                Game.interactable = false;
                Game.blocksRaycasts = false;
            }

            if (Result != null)
            {
                Result.alpha = 0.0f;
                Result.interactable = false;
                Result.blocksRaycasts = false;
            }
            if (CustomGame != null)
            {
                CustomGame.alpha = 0.0f;
                CustomGame.interactable = false;
                CustomGame.blocksRaycasts = false;
            }
        }
        //existing func
        public static string FormatTime(double seconds)
        {
            float m = Mathf.Floor((int)seconds / 60);
            float s = (float)seconds - (m * 60);
            string mStr = m.ToString("00");
            string sStr = s.ToString("00.000");
            return $"{mStr}:{sStr}";
        }
        public static string FormatTimeHoursMinsSec(double seconds)
        {
            TimeSpan t = TimeSpan.FromSeconds( seconds );
            return t.Days>0 ? $"{t.Days:D2}d:{t.Hours:D2}h:{t.Minutes:D2}m:" : $"{t.Hours:D2}h:{t.Minutes:D2}m";
        }
        //existing func

        private static IEnumerator ShowCanvas(CanvasGroup group, float target)
        {
            if (group == null) yield break;
            float startAlpha = group.alpha;
            float t = 0.0f;
            group.interactable = target >= 1.0f;
            group.blocksRaycasts = target >= 1.0f;

            while (t < AnimationTime)
            {
                t = Mathf.Clamp(t + Time.deltaTime, 0.0f, AnimationTime);
                group.alpha = Mathf.SmoothStep(startAlpha, target, t / AnimationTime);
                yield return null;
            }
        }
        //additional UI show/hides following existing structure
    
        public void ShowCustomGameMenu()
        {
            StartCoroutine(ShowCanvas(CustomGame, 1.0f));
        }
        public void HideCustomGameMenu()
        {
            StartCoroutine(ShowCanvas(CustomGame, 0.0f));
        }
        public void ShowHighscoresMenu()
        {
            StartCoroutine(ShowCanvas(HighscoresMenu, 1.0f));
            HighscoresMenu.GetComponent<Highscores>().Refresh();
        }
        public void HideHighscoresMenu()
        {
            StartCoroutine(ShowCanvas(HighscoresMenu, 0.0f));
        }

        public void ShowBackground()
        {
            StartCoroutine(ShowCanvas(BackGround, 1.0f));
        }
        public void HideBackground()
        {
            StartCoroutine(ShowCanvas(BackGround, 0.0f));
        }
        public void ShowLevelSelect()
        {
            StartCoroutine(ShowCanvas(LevelSelect, 1.0f));
        }
        public void HideLevelSelect()
        {
            StartCoroutine(ShowCanvas(LevelSelect, 0.0f));
        }
    
        public void ShowMiniMap()
        {
            StartCoroutine(ShowCanvas(MiniMap, 1.0f));
        }
        public void HideMiniMap()
        {
            StartCoroutine(ShowCanvas(MiniMap, 0.0f));
        }
    
        public void ShowAuxButtons()
        {
            StartCoroutine(ShowCanvas(AuxButtons, 1.0f));
        }
        public void HideAuxButtons()
        {
            StartCoroutine(ShowCanvas(AuxButtons, 0.0f));
        }
        public void ShowHelp()
        {
            StartCoroutine(ShowCanvas(Help, 1.0f));
        }
        public void HideHelp()
        {
            StartCoroutine(ShowCanvas(Help, 0.0f));
        }
        public void ShowSettings()
        {
            StartCoroutine(ShowCanvas(Settings, 1.0f));
        }
        public void HideSettings()
        {
            StartCoroutine(ShowCanvas(Settings, 0.0f));
        }

        public void HideStart()
        {
            CloseImmediate(HighscoresMenu);
            CloseImmediate(LevelSelect);
            CloseImmediate(MiniMap);

        }

        private void CloseImmediate(CanvasGroup group)
        {
            if (group == null) return;
            group.interactable = false;
            group.blocksRaycasts = false;
            group.alpha = 0.0f;
        }


    }
}
