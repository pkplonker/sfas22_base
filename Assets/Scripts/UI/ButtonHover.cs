using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class ButtonHover : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
    
    {
        private Button b;
        [SerializeField] private float scale = 1.1f;
        [SerializeField] private float duration = 0.1f;
        [SerializeField] private Ease ease = Ease.InFlash;

        private void OnValidate()=>GetReferences();
        private void GetReferences()
        {
            if (b != null ) return;
            if (TryGetComponent(out b)) return;
            Debug.LogError("ButtonHover must be attached to a button");
        }

        public void OnPointerEnter(PointerEventData eventData) => Scale(true);
        public void OnPointerExit(PointerEventData eventData) => Scale(false);

        private void Scale(bool hover)=>transform.DOScale(Vector3.one * (hover ?scale:1.0f), duration).SetEase(ease);
        
   
    }
}
