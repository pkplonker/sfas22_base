using System;
using Game;
using UnityEngine;

//handles all PauseMenu Functionality
namespace UI
{
	public class PauseMenu : MonoBehaviour
	{
		[SerializeField] GameObject menu;
		[SerializeField] private Game.Game game;
		[SerializeField] private Board board;
		[SerializeField] private PauseState pauseState;
		private bool isActive;
		public static event Action<bool> OnPause;
		private void Awake()=>menu.SetActive(false);
		

		private void OnEnable()
		{
			board.OnBoardRebuild += SetActive;
			game.OnGameOver += SetInactive;
		}

		private void SetActive(int arg1, int arg2)
		{
			isActive = true;
			menu.SetActive(false);
		}

		private void SetInactive()
		{
			menu.SetActive(false);
			isActive = false;
		}

		private void OnDisable()
		{
			board.OnBoardRebuild -= SetActive;
			game.OnGameOver -= SetInactive;
		}

		// Update is called once per frame
		private void Update()
		{
			if (!Input.GetKeyDown(KeyCode.Escape) || !isActive) return;
			SetMenuStates(!menu.activeSelf);
		}

		private void SetMenuStates(bool state)
		{
			menu.SetActive(state);
			pauseState.isPaused = state;
			OnPause?.Invoke(!state);
		}

		public void Resume()
		{
			menu.SetActive(false);
			pauseState.isPaused=false;
			OnPause?.Invoke(true);
		}

		public void Menu()=>game.GameOverLoss(false);
		public void ShowMenu()=>menu.SetActive(true);
		public void HideMenu()=>menu.SetActive(true);

		 }
}