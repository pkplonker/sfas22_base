using SO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	//Functionality relating to individual level select buttons
	public class LevelSelectButton : MonoBehaviour
	{
		[SerializeField] private Image[] stars;
		[SerializeField] private TextMeshProUGUI text;
		[SerializeField] private TextMeshProUGUI starsRequired;
		private ScoreSO scoreSo;
		private Game.Game game;
		private SO.Level level;
		private bool listenerAdded;
		private Image background;
		private void Awake()
		{
			text = GetComponentInChildren<TextMeshProUGUI>();
			UpdateStars(false);

			background = GetComponent<Image>();
		}

		//artificial constructor
		public void Init(SO.Level level, ScoreSO scoreSo, Game.Game game)
		{
			this.scoreSo = scoreSo;
			this.game = game;
			this.level = level;
			text.text = ((int) level.level - 1).ToString();
			UpdateStarsRequired();
			UpdateStars(true);
			UpdateButton();
		}

		private void UpdateStarsRequired()=>starsRequired.text = scoreSo.GetStars()+"/"+level.requiredStars;
		

		private void UpdateStars(bool state)
		{
			for (int i = 0; i < (state?level.starsEarned:stars.Length); i++)
			{
				stars[i].gameObject.SetActive(state);
			}
		}

		private void UpdateButton()
		{
			UpdateStarsRequired();
			if (scoreSo.GetStars() >= level.requiredStars)
			{
				if (!listenerAdded) AddButtonListener();
				UpdateStars(true);
				SetColor(Color.white);
			}
			else SetColor(Color.grey);
			
		}

		private void SetColor(Color color)=>background.color = color;

		private void AddButtonListener()
		{
			GetComponent<Button>().onClick.AddListener(() => game.NewGameFactory.OnClickedNewGame((int) level.level - 1));
			listenerAdded = true;
		}


		public void Clear()
		{
			UpdateStars(false);
			UpdateStars(true);
			UpdateButton();
		}
	}
}