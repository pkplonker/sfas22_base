using System.Collections.Generic;
using SO;
using UnityEngine;

namespace UI
{
	public class LevelSelect : MonoBehaviour
	{
		// Start is called before the first frame update
		private UI _ui;
		[SerializeField] private ScoreSO scoreSO;
		[SerializeField] private LevelData levelData;
		[SerializeField] private GameObject buttonPrefab;
		[SerializeField] private Transform container;
		private List<LevelSelectButton> buttons = new List<LevelSelectButton>();
		[SerializeField] private Game.Game game;

		private void OnEnable()
		{
			scoreSO.OnStarsChanged += RefreshButtons;
			levelData.OnStarsChanged += RefreshButtons;
			scoreSO.OnClearScores += RefreshButtons;
		}
		
		private void OnDisable()
		{
			scoreSO.OnStarsChanged -= RefreshButtons;
			levelData.OnStarsChanged -= RefreshButtons;
			scoreSO.OnClearScores -= RefreshButtons;
		}

		private void Awake()
		{
			_ui = GetComponentInParent<global::UI.UI>();
			GenerateButtons();
		}

		private void GenerateButtons()
		{
			foreach (var level in levelData.levels)
			{
				if (level.level == ScoreSO.Levels.Custom || level.level == ScoreSO.Levels.Null) continue;
				GameObject button = Instantiate(buttonPrefab, container);
				LevelSelectButton buttonScript = button.GetComponent<LevelSelectButton>();
				buttons.Add(buttonScript);
				buttonScript.Init(level, scoreSO,game);
			}
		}


		public void BackButton()
		{
			_ui.HideLevelSelect();
			_ui.ShowMenu();
		}

		private void RefreshButtons(int stars)
		{
			foreach (var button in buttons)
			{
				button.Clear();
			}
		}
		private void RefreshButtons()
		{
			foreach (var button in buttons)
			{
				button.Clear();
			}
		}
		
		
	
	}
}