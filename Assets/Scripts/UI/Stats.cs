using SO;
using TMPro;
using UnityEngine;
//Stats "box" on highscores screen functionality
public class Stats : MonoBehaviour
{
   [SerializeField] private ScoreSO scoreSo;
   [SerializeField] private TextMeshProUGUI gamesPlayedText;
   [SerializeField] private TextMeshProUGUI gamesWonText;
   [SerializeField] private TextMeshProUGUI timePlayedText;
   [SerializeField] private TextMeshProUGUI starsEarnedText;

   private void OnEnable()
   {
      scoreSo.OnScoreAdded += UpdateStats;
      scoreSo.OnClearScores += ClearStats;
      UpdateStats();
   }
   private void OnDisable()
   {
      scoreSo.OnScoreAdded -= UpdateStats;
      scoreSo.OnClearScores -= ClearStats;
   }
   
   void ClearStats()
   {
      gamesPlayedText.text = "0";
      gamesWonText.text = "0";
      timePlayedText.text = "0";
      starsEarnedText.text = "0";
   }

   void UpdateStats()
   {
      gamesPlayedText.text = scoreSo.GetGamesPlayed().ToString();
      gamesWonText.text = scoreSo.GetGamesWon().ToString();
      timePlayedText.text = UI.UI.FormatTimeHoursMinsSec(scoreSo.GetTimePlayed());
      starsEarnedText.text = scoreSo.GetStars().ToString();
   }
}
