using System.Collections.Generic;
using SO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Handles func relating to results screen
public class ResultsUI : MonoBehaviour
{
    [SerializeField] private List<Image> starImages;
    [SerializeField] private TMP_Text ResultText;
    [SerializeField] private TextMeshProUGUI currentTimeText;
    [SerializeField] private TextMeshProUGUI fastestTimeText;
    [SerializeField] private List<Image> emptyStarImages;
    private static readonly string[] ResultTexts = { "Oh no! You disturbed a \n 'Hibernating Hedgehog!'", "You Win!!\n The garden looks great!", "You Win!!\n A new fastest time!" };
    private static readonly string currentTimeString = "Your time: ";
    private static readonly string fastestTimeString="Fastest time: ";
 
    //Show called at end of game
    public void Show(bool success, Score score )
    {
        ShowResultsText(success, score);
        if (ShowTimeTexts(score)) return;
        SetStarsDisplay(score);
    }
    //Sets star images active based on number of stars
    private void SetStarImages(int stars=0)
    {
        for (int i = 0; i < starImages.Count; i++)
        {
            starImages[i].enabled = stars-1>=i;
        }
        
    }
    //Sets Empty star images
    private void SetEmptyStarImages(bool state)
    {
        foreach (var t in emptyStarImages)
        {
            t.enabled = state;
        }
        SetStarImages();
    }
  

    private void ShowResultsText(bool success, Score score)
    {
        if (ResultText == null) return;
        ResultText.text = ResultTexts[ScoreSO.IsFastestTime(score) ? 2 : success ? 1 : 0];
        if (score == null)
        {
            SetEmptyStarImages(true);
            SetStarImages();
        }
    }
    //Clear stars if unsupported game type
    private void SetStarsDisplay(Score score)
    {
        if (score.GetLevel() == ScoreSO.Levels.Null || score.GetLevel() == ScoreSO.Levels.Custom)
        {
            SetEmptyStarImages(false);
            return;
        }

        SetStarImages(score.GetStars());
    }
    //Shows times based on score
    private bool ShowTimeTexts(Score score)
    {
        if (currentTimeText != null && fastestTimeText != null && score != null)
        {
            SetTextsActive();
            currentTimeText.text = currentTimeString + UI.UI.FormatTime(score.GetTime());
            fastestTimeText.text = fastestTimeString + UI.UI.FormatTime(ScoreSO.bestTimes[score.GetLevel()]);
        }
        else
        {
            SetTextsActive(false);
            return true;
        }
        return false;
    }
    //Enables/disables text based on passed bool
    private void SetTextsActive(bool state = true)
    {
        currentTimeText.enabled = state;
        fastestTimeText.enabled = state;
    }
}
