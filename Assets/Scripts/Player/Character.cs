using System;
using System.Collections;
using Game;
using UI;
using UnityEngine;

namespace Player
{
	public class Character : MonoBehaviour
	{
		[SerializeField] private Board board;
		[SerializeField] private Game.Game game;
		[SerializeField] private bool isActive = false;
		[SerializeField] private float moveSpeed = 3f;
		[SerializeField] private float lookSpeedX;
		[SerializeField] private float lookSpeedY;
		[SerializeField] private float minLookLimit;
		[SerializeField] private float maxLookLimit;
		private float xRot;
		private Camera playerCamera;
		private CharacterController characterController;
		[SerializeField] private GameObject reticle;
		private Animator animator;
		private void Awake()
		{
			characterController = GetComponent<CharacterController>();
			playerCamera = GetComponentInChildren<Camera>();
			animator = GetComponent<Animator>();
			SetActive(false);
		}

		private void OnEnable()
		{
			board.OnBoardRebuild += OnBoardRebuild;
			game.OnGameOver += GameOver;
			PauseMenu.OnPause += SetActive;
		}

		private void OnDisable()
		{
			board.OnBoardRebuild -= OnBoardRebuild;
			game.OnGameOver -= GameOver;
			PauseMenu.OnPause-= SetActive;

		}
		//Actions when game is over.
		private void GameOver()
		{
			LockCursor(false);
			SetActive(false);
		}
		//moves char to correct location for new game layout
		private void OnBoardRebuild(int width, int height)
		{
			SetActive(false);
			SetDefaultPosition(width);
			SetActive();
		}

		private void SetDefaultPosition(int width)
		{
			gameObject.transform.position = new Vector3(width / 2, 0.18f, 0f);
			transform.eulerAngles = new Vector3(0, 5f, 0);
		}


		public bool GetActiveStatus()=> isActive;
		
		void SetActive(bool state = true)
		{
			characterController.enabled = state;
			LockCursor(state);
			if (state) StartCoroutine(DelayMovementStart());
			else isActive = false;
			animator.SetBool("isMoving",false);

		}

		IEnumerator DelayMovementStart()
		{
			yield return new WaitForSeconds(.3f);
			isActive = true;
		}
		//looks and moves
		private void Update()
		{
			if (!isActive) return;
			Vector3 moveDirection = GetInput();
			MouseLook();
			Move(moveDirection);
		}

		//rotates camera in x, and turns player around y
		private void MouseLook()
		{
			//up and down look
			playerCamera.transform.localRotation =
				Quaternion.Euler(Mathf.Clamp(xRot -= Input.GetAxis("Mouse Y") * lookSpeedY, minLookLimit, maxLookLimit), 0,
					0);
			//left right rotation
			transform.localRotation *= Quaternion.Euler(0f, Input.GetAxis("Mouse X") * lookSpeedX, 0f);
		}
	
		//uses character controller to move
		private void Move(Vector3 moveDirection)
		{
			moveDirection = new Vector3(moveDirection.x, 0, moveDirection.z);
			characterController.Move(  moveDirection * Time.deltaTime);
		}
		//input generated from camera angle to get direction and * inputs and move speed
		private Vector3 GetInput()
		{
		
			Vector3 moveDirection = Quaternion.Euler(0, playerCamera.transform.eulerAngles.y, 0) * 
			                        new Vector3(Input.GetAxis("Horizontal") * moveSpeed, 0,
				                        Input.GetAxis("Vertical") * moveSpeed);
			if (moveDirection.y != 0 || moveDirection.z != 0)
			{
				animator.SetBool("isMoving",true);
				animator.SetFloat("moveY",moveDirection.x);
				animator.SetFloat("moveX",moveDirection.z);
			}
			else animator.SetBool("isMoving",false);
			return moveDirection;
		}
		//locks cursor in game
		private void LockCursor(bool isLocked)
		{
			if (isLocked)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
				reticle.SetActive(true);
			}
			else
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
				reticle.SetActive(false);
			}
		}

		public void PlayAnim()=>animator.SetTrigger("action");
		

		private void FootL()
		{
		
		}

		void FootR()
		{
		
		}
	}
}