using UnityEngine;

namespace Player
{
	public class PlayerCamera : MonoBehaviour
	{
		[SerializeField] private float mouseSpeed;
		[SerializeField] private float rotMin;
		[SerializeField] private float rotMax;
		private float xRot;
		private Character character;

		private void Awake() => character = GetComponentInParent<Character>();
		
		private void Update()
		{
			if (!character.GetActiveStatus()) return;
			GetMouseInput(out var mouseY, out var mouseX);
			PerformRotation(mouseY, mouseX);
		}

		private void PerformRotation(float mouseY, float mouseX)
		{
			xRot -= mouseY;
			xRot = Mathf.Clamp(xRot, rotMin, rotMax);
			transform.localRotation = Quaternion.Euler(xRot, 0, 0);
			character.transform.Rotate(Vector3.up * mouseX);
		}

		private void GetMouseInput(out float mouseY, out float mouseX)
		{
			mouseX = Input.GetAxis("Mouse X") * mouseSpeed * Time.deltaTime;
			mouseY = Input.GetAxis("Mouse Y") * mouseSpeed * Time.deltaTime;
		}
	}
}