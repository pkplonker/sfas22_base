using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SO;
using UnityEngine;

namespace SaveSystem
{
    public class LoadSystem : MonoBehaviour
    {
        public static LoadSystem instance;
        public static event Action<SaveData> OnDataLoaded; 
        //singleton
        void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(this);
        }
        //opens file & bf, loads and invokes event
        public static SaveData Load()
        {
            if (!File.Exists(Application.persistentDataPath + "/gamesave.data")) return null;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.data", FileMode.Open);
            SaveData save = (SaveData)bf.Deserialize(file);
            file.Close();
            OnDataLoaded?.Invoke(save);
            return save;
        }
    }
}
