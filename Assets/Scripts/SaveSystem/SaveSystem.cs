using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SO;
using UnityEngine;

namespace SaveSystem
{
	public class SaveSystem : MonoBehaviour
	{
		public static SaveSystem instance;
		[SerializeField] private ScoreSO scoreSo;
		public static event Action OnDataSaved; 

		void Awake()
		{
			if (instance == null) instance = this;
			else Destroy(this);
		}
	
		//opens file & bf, saves and invokes event
		public void ClearAndSave()
		{
			scoreSo.Clear();
			Save();
		}
		

		public void Save()
		{
			SaveData saveData = scoreSo.GenerateSaveData();
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream file = File.Create(Application.persistentDataPath + "/gamesave.data");
			binaryFormatter.Serialize(file, saveData);
			file.Close();
			OnDataSaved?.Invoke();
		}
		
		//opens file & bf, saves passed in data and invokes event
		public void Save(SaveData saveData)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream file = File.Create(Application.persistentDataPath + "/gamesave.data");
			Debug.Log(Application.persistentDataPath);
			binaryFormatter.Serialize(file, saveData);
			file.Close();
			OnDataSaved?.Invoke();
		}
	
	}
}
