using System;
using System.Collections.Generic;
using SO;

namespace SaveSystem
{
	[Serializable]
//Class for containing saveData
	public class SaveData
	{
		public List<Score> scoreSavaData;
		public int stars;
		public int gamesPlayed;
		public int gamesWon;
		public double timePlayed;
		public  Dictionary<ScoreSO.Levels,double> bestTimes = new Dictionary<ScoreSO.Levels, double>();
		//Generates saveData from currentScores
		public SaveData(ScoreSO scoreSo)
		{
			
			scoreSavaData = new List<Score>();
			foreach (var score in scoreSo.scores)
			{
				scoreSavaData.Add(score);
			}

			gamesPlayed = scoreSo.GetGamesPlayed();
			gamesWon = scoreSo.GetGamesWon();
			timePlayed = scoreSo.GetTimePlayed();
			stars = scoreSo.GetStars();
			bestTimes = ScoreSO.GetBestTimes();
		}

		public SaveData()
		{
			scoreSavaData = new List<Score>();
			gamesPlayed = 0;
			gamesWon = 0;
			timePlayed = 0;
			stars = 0;
		}
	}
}