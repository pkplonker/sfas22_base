using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//Controls Audio Throughout game
public class AudioManager : MonoBehaviour
{
	[SerializeField] private List<AudioClip> musicClips;
	[SerializeField] private List<AudioClip> clickSounds;
	[SerializeField] private AudioSource sfxSource;
	[SerializeField] private AudioSource uiSource;
	private AudioSource musicSource;

	private void Awake()
	{
		musicSource = GetComponent<AudioSource>();
		PlayMenuMusic();
	}


	void PlayMenuMusic()
	{
		musicSource.loop = true;
		musicSource.clip = musicClips[Random.Range(0, musicClips.Count)];
		musicSource.Play();
	}
	

	void PlayUI()=>uiSource.PlayOneShot(clickSounds[Random.Range(0, clickSounds.Count - 1)]);
	

	void PlaySFX(AudioClip clip)
	{
		if (clip != null) sfxSource.PlayOneShot(clip);
	}
}