using System;
using System.Collections;
using SO;
using UI;
using UnityEngine;

namespace Game
{
	public class Game : MonoBehaviour
	{
		[SerializeField] Board _board;
		private UI.UI _ui;
		internal bool _gameInProgress;
		[SerializeField] private ScoreSO resultStore;
		private CustomGameUI _customGameUI;
		internal ScoreSO.Levels currentLevel;
		[SerializeField] private LevelData levelData;
		[SerializeField] private PauseState pauseState;
		private double gameTime;
		private NewGameFactory newGameFactory;
		private SO.Level currentCustomLevel;


		public event Action OnGameOver;

		public NewGameFactory NewGameFactory => newGameFactory;

		//event subscription
		private void OnEnable() => _customGameUI.OnCustomGame += NewGameFactory.OnNewCustomGame;

		private void OnDisable() => _customGameUI.OnCustomGame -= NewGameFactory.OnNewCustomGame;


		//given func - modified for level management

		//used to init a new custom game

		//ui button for showing custom game menu
		public void OnClickedCustomGame()
		{
			_ui.HideMenu();
			_ui.ShowCustomGameMenu();
		}
		public void OnClickedExit()
		{
			SaveSystem.SaveSystem.instance.Save();
			Application.Quit();
		}

		//given func - modified for additional UI
		public void OnClickedReset()
		{
			if (_board != null)
			{
				_board.Clear();
				ResetTime();
			}

			if (_ui != null)
			{
				_ui.HideResult();
				_ui.ShowMenu();
				_ui.HideGame();
				_ui.HideMiniMap();
			}
		}

		public void OnClickedReplay()
		{
			if (_board != null)
			{
				_board.Clear();
				ResetTime();
			}

			if (currentLevel == ScoreSO.Levels.Custom)
				NewGameFactory.OnNewCustomGame(currentCustomLevel);
			else
				NewGameFactory.OnClickedNewGame((int) currentLevel - 1);
		}

		//given func - modified for additional UI
		private void Awake()
		{
			Transform parent = transform.parent;
			_ui = parent.GetComponentInChildren<UI.UI>();
			_gameInProgress = false;
			_customGameUI = parent.GetComponentInChildren<CustomGameUI>();
			newGameFactory = new NewGameFactory(this, _board, _ui, levelData);
		}

		private void Start()
		{
			if (_ui == null) return;
			_ui.ShowMenu();
			_ui.HideStart();
		}

		private void Update()
		{
			if (_ui != null && !pauseState.isPaused)
				gameTime += Time.deltaTime;
			_ui.UpdateTimer(_gameInProgress ? gameTime : 0.0);
		}

		//given function - modified for storing scores
		internal void BoardEvent(Board.Event eventType)
		{
			if (eventType == Board.Event.ClickedDanger && _ui != null) GameOverLoss();
			if (eventType == Board.Event.Win && _ui != null) GameOverWin();
			if (!_gameInProgress)
			{
				_gameInProgress = true;
				ResetTime();
			}
		}

		private void GameOverWin()
		{
			Score score = null;
			if (resultStore != null)
			{
				score = new Score(gameTime,
					_board.GetWidthAndHeight(), _board.GetNumberOfDangerousBoxes(), currentLevel, levelData);
				resultStore.AddTime(score);
			}

			OnGameOver?.Invoke();
			_ui.HideGame();
			_ui.ShowBackground();
			_ui.HideMiniMap();
			_ui.ShowResult(true, score);
		}

		public void GameOverLoss(bool showResult = true)
		{
			StartCoroutine(GameOverLossDelay(showResult));
		}

		private IEnumerator GameOverLossDelay(bool showResult)
		{
			OnGameOver?.Invoke();
			yield return new WaitForSeconds(1f);
			_ui.ShowBackground();
			if (showResult) _ui.ShowResult( false, null);
			else _ui.ShowMenu();
			_ui.HideGame();
			_ui.HideMiniMap();
			if (resultStore != null)
			{
				resultStore.RecordLoss(gameTime);
			}
		}

		//restarts time
		public void ResetTime()
		{
			pauseState.isPaused = false;
			gameTime = 0;
		}

		//highscores button
		public void OnShowHighScores()
		{
			_ui.ShowHighscoresMenu();
			_ui.HideMenu();
		}

		public void OnShowLevelSelect()
		{
			_ui.HideMenu();
			_ui.ShowLevelSelect();
		}
	}
}