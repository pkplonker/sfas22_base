using System.Runtime.CompilerServices;
using SO;

namespace Game
{
	public class NewGameFactory
	{
		private readonly Game _game;
		private readonly Board _board;
		private readonly UI.UI _ui;
		private readonly LevelData _levelData;

		public NewGameFactory(Game game, Board board, UI.UI ui,LevelData levelData)
		{
			_game = game;
			_board = board;
			_ui = ui;
			_levelData = levelData;
		}

		private void CreateLayout()
		{
			if (_board != null)
			{
				_board.Setup(_game.BoardEvent);
			}
		}

		public void OnClickedNewGame(int level = 1)
		{
			_game.ResetTime();
			if (_board != null)
			{
				_game.currentLevel = (ScoreSO.Levels) level + 1;
				_board.CustomGameConfig(_levelData.levels[level]);
				CreateLayout();
				_board.RechargeBoxes();
			}

			if (_ui != null) CloseNewUI();
			_game._gameInProgress = false;
		}

		private void CloseNewUI()
		{
			_ui.HideMenu();
			_ui.HideLevelSelect();
			_ui.ShowGame();
			_ui.HideBackground();
			_ui.ShowMiniMap();
			_ui.HideSettings();
			_ui.HideResult();
			_ui.HideCustomGameMenu();

		}

		internal void OnNewCustomGame(SO.Level level)
		{
			_game.ResetTime();
			if (_board != null)
			{
				_board.CustomGameConfig(level);
				CreateLayout();
				_board.RechargeBoxes();
				_ui.HideBackground();
				_game.currentLevel = ScoreSO.Levels.Custom;
			}
			CloseNewUI();
			_game._gameInProgress = false;
		}

		
	}
}