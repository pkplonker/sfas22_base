using System;
using System.Collections;
using Player;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game
{
	public class Box : MonoBehaviour
	{
		#region Variables

		

		[FormerlySerializedAs("DangerColors")] [SerializeField]
		private Color[] dangerColors = new Color[8];
		[FormerlySerializedAs("Danger")] [SerializeField]
		private GameObject danger;
		[SerializeField] private GameObject flag;
		[SerializeField] private Material clickedMaterial;
		[SerializeField] private float colorChangeFactor = 1f;
		[SerializeField] GameObject leafParticle;

		public int RowIndex { get; private set; }
		public int ID { get; private set; }
		public int DangerNearby { get; private set; }
		public bool IsDangerous { get; private set; }
		public bool IsActive { get; private set; } = true;
		public bool IsRevealed{ get; private set; }
		private Character character;
		private Coroutine delayCor;
		private Game game;
		private bool isGameOver = true;
		private Color defaultColor;
		private TMP_Text textDisplay;
		private Material defaultMaterial;
		private PauseState pauseState;
		private Action<Box> changeCallback;
		private MeshRenderer meshRenderer;	
		#endregion

		//existing func with flag addition
		private void OnDisable()
		{
			game.OnGameOver -= StopDelayCor;
		}

		public void Setup(int id, int row,  Character character, Game game, PauseState pauseState)
		{
			ResetState();
			ID = id;
			RowIndex = row;
			SetFlag();
			this.character = character;
			StopDelayCor();
			this.game = game;
			this.game.OnGameOver += StopDelayCor;
			this.pauseState = pauseState;
			IsRevealed = false;
			
		}
		//existing func

		public void Charge(int dangerNearby, bool danger, Action<Box> onChange)
		{
			changeCallback = onChange;
			DangerNearby = dangerNearby;
			IsDangerous = danger;
			ResetState();
		}
		//existing func

		public void Reveal()
		{
			IsActive = false;
			DisplayClickedVisuals();
		}


		private void OnMouseOver()
		{
			if (!IsActive) return;
			if (Input.GetKeyDown(KeyCode.Mouse0)) OnClick();
			else if (Input.GetKeyDown(KeyCode.Mouse1)) RightClick();
		}


		//existing func

		void OnClick()
		{
			if (!CheckRange()) return;
			if (delayCor == null && !isGameOver && !pauseState.isPaused && !IsRevealed)
				delayCor = StartCoroutine(ClickDelay());
		}

		private IEnumerator ClickDelay()
		{
			if (character != null) character.PlayAnim();
			yield return new WaitForSeconds(.2f);
			if (flag != null) flag.SetActive(false);
			IsActive = false;
			if (IsDangerous && danger != null) danger.SetActive(true);
			else DisplayClickedVisuals();
			delayCor = null;
			HandleEventCallback();
		}

		private void HandleEventCallback()
		{
			if (!isGameOver && !IsRevealed)
			{
				IsRevealed = true;
				changeCallback?.Invoke(this);
			}
		}

		private bool CheckRange() => Vector3.Distance(gameObject.transform.position, character.transform.position) < 3f;


		private void DisplayClickedVisuals()
		{
			StartCoroutine(VisualDisplayDelay());
		}

		private IEnumerator VisualDisplayDelay()
		{
			leafParticle.SetActive(true);
			HandleEventCallback();
			meshRenderer.material = clickedMaterial;
			textDisplay.enabled = true;
			while (meshRenderer.material.color.r > 0.05f)
			{
				meshRenderer.material.color -= new Color(Time.deltaTime * colorChangeFactor,
					Time.deltaTime * colorChangeFactor, Time.deltaTime * colorChangeFactor);
				yield return null;
			}
		}

		//func on rightclick
		private void RightClick()
		{
			if (!CheckRange()) return;
			SetFlag(!flag.activeSelf);
		}

		private void Awake()
		{
			meshRenderer = GetComponent<MeshRenderer>();
			defaultMaterial = meshRenderer.material;
			defaultColor = defaultMaterial.color;
			textDisplay = GetComponentInChildren<TextMeshProUGUI>();
			ResetState();
		}

		//existing func with addition of flag control
		public void ResetState()
		{
			ResetMaterial();
			StopDelayCor();
			if (danger != null) SetFlag();
			ResetTextDisplay();
			danger.SetActive(false);
			IsActive = true;
			isGameOver = false;
			IsRevealed = false;
		}

		private void ResetTextDisplay()
		{
			if (textDisplay != null)
			{
				if (DangerNearby > 0)
				{
					textDisplay.text = DangerNearby.ToString("D");
					textDisplay.color = dangerColors[DangerNearby - 1];
				}
				else textDisplay.text = string.Empty;
				textDisplay.enabled = false;
			}
		}

		private void ResetMaterial()
		{
			meshRenderer.material = defaultMaterial;
			meshRenderer.material.color = defaultColor;
		}

		private void StopDelayCor()
		{
			isGameOver = true;
			if (delayCor == null) return;
			StopCoroutine(delayCor);
			delayCor = null;
		}

		//toggle showing flag
		public void SetFlag(bool state = false)
		{
			flag.SetActive(state);
		}
	}
}