using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game
{
	public class Board : MonoBehaviour
	{
		public enum Event
		{
			ClickedBlank,
			ClickedNearDanger,
			ClickedDanger,
			Win
		}
		public event Action<int, int> OnBoardRebuild;

		[FormerlySerializedAs("BoxPrefab")] [SerializeField] private Box boxPrefab;
		private int width = 10;
		private int height = 10;
		private int numberOfDangerousBoxes = 10;
		[SerializeField] private Camera mapCamera;
		private Box[] grid;
		private Vector2Int[] neighbours;
		private Action<Event> clickEvent;
		[SerializeField] private Character character;
		[SerializeField] private Game game;
		[SerializeField] private PauseState pauseState;

		public int GetWidthAndHeight() => width * height;
		public int GetNumberOfDangerousBoxes() => numberOfDangerousBoxes;
		
		//existing func
		public void Setup(Action<Event> onClickEvent)
		{
			clickEvent = onClickEvent;
			Clear();
		}

		//existing func
		public void Clear()
		{
			for (int row = 0; row < height; ++row)
			{
				for (int column = 0; column < width; ++column)
				{
					int index = row * width + column;
					grid[index].ResetState();
				}
			}
		}
//existing func

		public void RechargeBoxes()
		{
			Clear();
			int numberOfItems = width * height;
			List<bool> dangerList = new List<bool>(numberOfItems);
			for (int count = 0; count < numberOfItems; ++count)
			{
				dangerList.Add(count < numberOfDangerousBoxes);
			}

			dangerList.RandomShuffle();
			for (int row = 0; row < height; ++row)
			{
				for (int column = 0; column < width; ++column)
				{
					int index = row * width + column;
					grid[index].Charge(CountDangerNearby(dangerList, index), dangerList[index], OnClickedBox);
				}
			}
		}

		// clears placed flags
		private void ClearAllFlags()
		{
			foreach (Box box in grid)
			{
				box.SetFlag();
			}
		}

		//existing func - moved
		private void Init(bool rebuild)
		{
			SetCameraPosition();
			if (rebuild) BuildTiles();
			OnBoardRebuild?.Invoke(width, height);
		}

		private void BuildTiles()
		{
			ClearBoard();
			grid = new Box[width * height];
			GenerateNeighbours();
			for (int row = 0; row < width; ++row)
			{
				GameObject rowObj = new GameObject($"Row{row}");
				rowObj.transform.SetParent(transform);
				for (int column = 0; column < height; ++column)
				{
					GenerateGridCells(row, column, rowObj);
				}
			}
		}

		private void GenerateGridCells(int row, int column, GameObject rowObj)
		{
			int index = row * width + column;
			CreateGridCellGameObject(rowObj, index);
			SetupGridCell(row, column, index);
		}

		private void SetupGridCell(int row, int column, int index)
		{
			grid[index].Setup(index, row,  character, game, pauseState);
			grid[index].transform.position = new Vector3(row, .1f, column);
			grid[index].name = $"ID{index}, Row{row}, Column{column}";
		}

		private void CreateGridCellGameObject(GameObject rowObj, int index)
		{
			grid[index] = Instantiate(boxPrefab, rowObj.transform);
			grid[index].transform.localScale = new Vector3(1, 0.1f, 1);
		}

		private void GenerateNeighbours()
		{
			neighbours = new Vector2Int[8]
			{
				new Vector2Int(-width - 1, -1),
				new Vector2Int(-width, -1),
				new Vector2Int(-width + 1, -1),
				new Vector2Int(-1, 0),
				new Vector2Int(1, 0),
				new Vector2Int(width - 1, 1),
				new Vector2Int(width, 1),
				new Vector2Int(width + 1, 1)
			};
		}

		private void SetCameraPosition()
		{
			mapCamera.transform.position = new Vector3(width % 2 > 0 ? width / 2 : width / 2 - 0.5f, width,
				(int) height % 2 > 0 ? height / 2 : height / 2 - 0.5f);
			mapCamera.orthographicSize = (float) width / 2;
		}
		//existing func

		private int CountDangerNearby(List<bool> danger, int index)
		{
			int result = 0;
			int boxRow = index / width;
			if (danger[index]) return result;
			for (int count = 0; count < neighbours.Length; ++count)
			{
				result = CountDangerNearbyMath(danger, index, count, boxRow, result);
			}

			return result;
		}

		private int CountDangerNearbyMath(List<bool> danger, int index, int count, int boxRow, int result)
		{
			int neighbourIndex = index + neighbours[count].x;
			int expectedRow = boxRow + neighbours[count].y;
			int neighbourRow = neighbourIndex / width;
			result += (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < danger.Count &&
			           danger[neighbourIndex])
				? 1
				: 0;
			return result;
		}

		//existing func modified for flags
		private void OnClickedBox(Box box)
		{
			Event clickEvent = Event.ClickedBlank;
			clickEvent = DetermineClickEvent(box, clickEvent);
			if (CheckForWin())
			{
				ClearAllFlags();
				clickEvent = Event.Win;
			}

			this.clickEvent?.Invoke(clickEvent);
		}

		private Event DetermineClickEvent(Box box, Event clickEvent)
		{
			box.SetFlag();
			if (box.IsDangerous)
			{
				ClearAllFlags();
				clickEvent = Event.ClickedDanger;
			}
			else if (box.DangerNearby > 0) clickEvent = Event.ClickedNearDanger;
			else ClearNearbyBlanks(box);

			return clickEvent;
		}
		//existing func

		private bool CheckForWin()
		{
			bool result = true;

			for (int count = 0; result && count < grid.Length; ++count)
			{
				if (!grid[count].IsDangerous && grid[count].IsActive)
				{
					result = false;
				}
			}

			return result;
		}

		//existing func
		private void ClearNearbyBlanks(Box box) => RecursiveClearBlanks(box, 0.2f);

		//existing func
		private void RecursiveClearBlanks(Box box, float delay)
		{
			if (box.IsDangerous) return;
			box.Reveal();
			box.SetFlag();
			if (box.DangerNearby != 0) return;
			foreach (var t in neighbours)
			{
				var neighbourIndex = IdentifyNeighbourIndex(box, t, out var correctRow, out var active);
				if (correctRow && active) StartCoroutine(DelayReveal(neighbourIndex, delay));
			}
		}

		private int IdentifyNeighbourIndex(Box box, Vector2Int t, out bool correctRow, out bool active)
		{
			int neighbourIndex = box.ID + t.x;
			int expectedRow = box.RowIndex + t.y;
			int neighbourRow = neighbourIndex / width;
			correctRow = expectedRow == neighbourRow;
			active = neighbourIndex >= 0 && neighbourIndex < grid.Length &&
			         grid[neighbourIndex].IsActive;
			return neighbourIndex;
		}

		IEnumerator DelayReveal(int neighbourIndex, float delay)
		{
			yield return new WaitForSeconds(delay);
			if (!grid[neighbourIndex].IsRevealed)
				RecursiveClearBlanks(grid[neighbourIndex], delay);
		}

		//takes custom game data and recreates board (for built is custom games i.e levels)
		public void CustomGameConfig(SO.Level level)
		{
			if (level.width == width && level.height == height && level.enemies == numberOfDangerousBoxes) Init(false);
			else
			{
				width = level.width;
				height = level.height;
				numberOfDangerousBoxes = level.enemies;
				Init(true);
			}
		}

		private void ClearBoard()
		{
			foreach (Transform child in transform)
			{
				Destroy(child.gameObject);
			}
		}
	}
}