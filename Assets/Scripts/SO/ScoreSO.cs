using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SaveSystem;
using UnityEngine;

namespace SO
{
	[CreateAssetMenu(fileName = "Scores", menuName = "Scores")]
	public class ScoreSO : ScriptableObject
	{
		//Enum for all avaliable levels.
		public enum Levels
		{
			Null,
			Custom,
			One,
			Two,
			Three,
			Four,
			Five,
			Six,
			Seven,
			Eight,
			Nine,
			Ten
		}

		public event Action<int> OnStarsChanged;
		public event Action OnScoreAdded;
		public event Action OnClearScores;


		public List<Score> scores;
		public static Dictionary<Levels, double> bestTimes;

		[SerializeField] int stars;
		[SerializeField] private int gamesPlayed;
		[SerializeField] private int gamesWon;
		[SerializeField] private double timePlayed;
		[SerializeField] LevelData levelData;

		//function to add score to list
		public bool AddTime(Score score)
		{
			if (scores == null) scores = new List<Score>();
			scores.Add(score);
			gamesPlayed++;
			gamesWon++;
			timePlayed += score.GetTime();
			OnScoreAdded?.Invoke();
			SaveSystem.SaveSystem.instance.Save();

			return IsFastestTime(score);
		}

		public static bool IsFastestTime(Score score)
		{
			if (score == null) return false;
			bestTimes ??= new Dictionary<Levels, double>();
			if (bestTimes.TryGetValue(score.GetLevel(), out double bestTime))
			{
				if (!(bestTime >= score.GetTime())) return false;
				bestTimes[score.GetLevel()] = score.GetTime();
			}
			else
			{
				bestTimes.Add(score.GetLevel(), score.GetTime());
				return true;
			}

			return true;
		}

		public static double GetFastestTime(Score score) => bestTimes[score.GetLevel()];


		public void IncreaseStars(int amount)
		{
			stars += amount;
			OnStarsChanged?.Invoke(stars);
		}

		//func to order list by time(within elements)
		public List<Score> Sort() => scores.OrderBy(x => x.GetTime()).ToList();

		public void Clear()
		{
			scores.Clear();
			ClearStars();
			gamesPlayed = 0;
			gamesWon = 0;
			timePlayed = 0;
			if (bestTimes != null) bestTimes.Clear();
			OnClearScores?.Invoke();
		}
		
		private void ClearStars()
		{
			stars = 0;
			levelData.ClearStars();
			OnStarsChanged?.Invoke(stars);
		}

		public SaveData GenerateSaveData() => new SaveData(this);

		public void LoadSaveData(SaveData saveData)
		{
			//Clear();
			if (saveData == null) return;
			foreach (var data in saveData.scoreSavaData)
			{
				scores.Add(data);
			}
			gamesPlayed = saveData.gamesPlayed;
			gamesWon = saveData.gamesWon;
			timePlayed = saveData.timePlayed;
			stars = saveData.stars;
			bestTimes = saveData.bestTimes;
			OnScoreAdded?.Invoke();
			OnStarsChanged?.Invoke(stars);
		}

		public static Dictionary<Levels, double> GetBestTimes() => bestTimes;

		public int GetStars() => stars;
		public int GetGamesPlayed() => gamesPlayed;
		public int GetGamesWon() => gamesWon;
		public double GetTimePlayed() => timePlayed;

		public void RecordLoss(double realtimeSinceStartupAsDouble)
		{
			timePlayed += realtimeSinceStartupAsDouble;
			gamesPlayed++;
			OnScoreAdded?.Invoke();
		}
	}


	//class to collate scoreData
	[Serializable]
	public class Score
	{
		[SerializeField] double time;
		[SerializeField] int gridSize;
		[SerializeField] int enemyCount;
		[SerializeField] float difficulty;
		[SerializeField] private ScoreSO.Levels level;
		[SerializeField] private int stars;

		//constructor
		public Score(double time, int gridSize, int enemyCount, ScoreSO.Levels level, LevelData levelData)
		{
			this.time = time;
			this.gridSize = gridSize;
			this.enemyCount = enemyCount;
			difficulty = (float) this.enemyCount / this.gridSize;
			this.level = level;
			stars = CalculateStars(time,level, levelData);
			if (levelData != null) levelData.StarsEarned(this);
		}

		private int CalculateStars(double time,ScoreSO.Levels _level, LevelData levelData)
		{
			foreach (var level in levelData.levels)
			{
				if (_level == level.level)
				{
					if (time <= level.threeStarTime) return 3;
					if (time <= level.twoStarTime) return 2;
					if (time <= level.oneStarTime) return 1;
				}
			}

			return 0;
		}
		//funcs to get
		public double GetTime() => time;
		public float GetDifficulty() => difficulty;
		public ScoreSO.Levels GetLevel() => level;
		public int GetStars() => stars;
	}
}