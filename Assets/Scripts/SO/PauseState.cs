using UnityEngine;

[CreateAssetMenu(fileName = "Pause State", menuName = "Pause State")]
public class PauseState : ScriptableObject
{
	public bool isPaused;
}