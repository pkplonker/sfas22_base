using System;
using System.Collections;
using System.Collections.Generic;
using SaveSystem;
using UnityEngine;

namespace SO
{
	[CreateAssetMenu(fileName = "LevelData", menuName = "LevelData")]
	public class LevelData : ScriptableObject
	{
		//list of all available levels
		public List<Level> levels;
		[SerializeField] private ScoreSO scores;
		public event Action OnStarsChanged;

		private void OnEnable()
		{
			LoadSystem.OnDataLoaded += LoadSaveData;
			scores.OnClearScores += ClearStars;
		}

		private void OnDisable()
		{
			LoadSystem.OnDataLoaded -= LoadSaveData;
			scores.OnClearScores -= ClearStars;
		}


		public void StarsEarned(Score score)
		{
			foreach (var level in levels)
			{
				if (score.GetLevel() != level.level) continue;
				if (score.GetStars() > level.GetStars())
				{
					int scoreIncrease = score.GetStars() - level.GetStars();
					level.SetStars(score.GetStars());
					scores.IncreaseStars(scoreIncrease);
				}
				break;
			}
		}

		private void LoadSaveData(SaveData saveData)
		{
			if (saveData == null) return;
			foreach (var score in saveData.scoreSavaData)
			{
				StarsEarned(score);
			}
		}

		public void ClearStars()
		{
			foreach (var level in levels)
			{
				level.SetStars(0);
			}
			OnStarsChanged?.Invoke();
		}
	}

//stores data for level
	[Serializable]
	public class Level
	{
		public ScoreSO.Levels level;
		public int width;
		public int height;
		public int enemies;
		public int starsEarned;
		public int requiredStars;
		public float oneStarTime;
		public float twoStarTime;
		public float threeStarTime;

		public Level(int _height, int _width, int _enemies, ScoreSO.Levels _level)
		{
			width = _width;
			enemies = _enemies;
			height = _height;
			level = _level;
		}

		public void SetStars(int stars) => starsEarned = stars;
		public int GetStars() => starsEarned;
		public int GetRequiredStars() => requiredStars;
	}
}