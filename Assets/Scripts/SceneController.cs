﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

//Controls Scene Transitions
public class SceneController : MonoBehaviour
{
	[SerializeField] private float timer = 3f;

	private void Start()
	{
		StartCoroutine(SceneCor()); //loads scene after set time
	}

	//delay loading new scene.
	IEnumerator SceneCor()
	{
		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(1);
		asyncOperation.allowSceneActivation = false;
		yield return new WaitForSeconds(timer);
		asyncOperation.allowSceneActivation = true;
		yield return new WaitForSeconds(.1f);
		SceneManager.UnloadSceneAsync(0);
		
	}
}
