using Game;
using UnityEngine;

namespace Level
{
    public class WallSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject wallPrefab;
        [SerializeField] private Transform wallContainer;
        private Board board;

        private void Awake()=> board = GetComponentInChildren<Board>();
        private void OnEnable()=>board.OnBoardRebuild += BuildWall;
        private void OnDisable()=>board.OnBoardRebuild -= BuildWall;
        //destroys all existing
        private void ClearWalls()
        {
            foreach (Transform child in wallContainer)
            {
                Destroy(child.gameObject);
            }
        }
    
        //func to build walls. Looks through width and height, instantiating at correct x's and Y's
        private void BuildWall(int width, int height)
        {
            ClearWalls();
            for (int i = 0; i < width; i++)
            {
                GenerateWallBlock(i, new Vector3(i, 0, -1f),0);
                GenerateWallBlock(i, new Vector3(i, 0, height),180);
            }
            for (int i = 0; i < height; i++)
            {
                GenerateWallBlock(i, new Vector3(-1f, 0, i),90);
                GenerateWallBlock(i, new Vector3(width, 0, i),270);
            }
        
        }

        private void GenerateWallBlock(int i, Vector3 position,int angle)
        {
            GameObject obj = Instantiate(wallPrefab, position, Quaternion.identity, wallContainer);
            SetValue(obj, angle);
        }

        private static void SetValue(GameObject obj,int angle)
        {
            obj.transform.eulerAngles = new Vector3(0, angle, 0);
            obj.transform.localScale = new Vector3(1, 0.5f, 1);

        }

   
    }
}
