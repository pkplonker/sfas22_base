using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
	public class PropSpawner : MonoBehaviour
	{
		[SerializeField] private List<GameObject> prefabs;
		[SerializeField] private List<GameObject> props = new List<GameObject>();
		[SerializeField] private Transform dynamicPropContainer;
		[SerializeField] private int maxPropX = 33;
		[SerializeField] private List<SpawnLocation> fixedSpawnLocations;
		[SerializeField] private Board board;
		private int currentPrefab = 0;
		private int previousWidth = 0;
		private int previousHeight = 0;

		private void OnEnable() => board.OnBoardRebuild += Spawn;
		private void OnDisable() => board.OnBoardRebuild -= Spawn;


//spawns objects at set locations offset by board width
		void FixedSpawn(int height, int width)
		{
			foreach (SpawnLocation item in fixedSpawnLocations)
			{
				GameObject newObj = Instantiate(item.prefab, item.position + new Vector3(width + 6, 0, 0),
					Quaternion.identity,
					dynamicPropContainer);
				newObj.transform.eulerAngles = item.eulerAngle;
				props.Add(newObj);
			}
		}

/* Spawns objects with dynamic position
 
 Creates location, box casts down to confirm space is clear. Size defined by mesh
 Increments up the prefab list and instantiates
 
 */
		public void SpawnProps(int height, int width)
		{
			Vector3 position = new Vector3(Random.Range(width, maxPropX), .13f, Random.Range(-20, maxPropX));

			GameObject obj = prefabs[currentPrefab];
			var ray = GenerateRays(position, obj);
			bool suitable = true;
			foreach (var hit in ray)
			{
				if (!hit.collider.CompareTag("GroundTarget"))
				{
					suitable = false;
					break;
				}
			}
			currentPrefab++;
			if (currentPrefab >= prefabs.Count) currentPrefab = 0;
			if (suitable)
			{
				GameObject newObj = Instantiate(obj, position, Quaternion.identity,
					dynamicPropContainer);
				newObj.transform.eulerAngles = new Vector3(0, 180, 0);
				props.Add(newObj);
			}
		}

		private static RaycastHit[] GenerateRays(Vector3 position, GameObject obj)
		{
			RaycastHit[] ray = Physics.BoxCastAll((position + new Vector3(0, 20, 0)),
				obj.GetComponent<Renderer>().bounds.max + new Vector3(0.5f, 0.5f, 0.5f), Vector3.down);
			return ray;
		}

		//destroys each existing prop
		public void DestroyProps()
		{
			currentPrefab = 0;
			foreach (GameObject prop in props)
			{
				Destroy(prop);
			}
		}

//spawns new props
		void Spawn(int width, int height)
		{
			currentPrefab = 0;
			StartCoroutine(Spawner(width, height));
		}

		//coroutine as was having overlapping issues if attempted all in one frame.
		IEnumerator Spawner(int width, int height)
		{
			if (height != previousHeight || width != previousWidth)
			{
				currentPrefab = 0;
				DestroyProps();
				FixedSpawn(height, width);
				yield return new WaitForEndOfFrame();
				for (int i = 0; i < 500; i++)
				{
					SpawnProps(height, width);
				}
			}
		}
	}

	[Serializable]
	public class SpawnLocation
	{
		public GameObject prefab;
		public Vector3 position;
		public Vector3 eulerAngle;
	}
}