using UnityEngine;
using UnityEditor;
using SaveSystem;
using SO;

//Debug tools
public class DebugTools : MonoBehaviour
{
	[MenuItem("Stuart Custom Debug/Save")]
	static void ForceSave()
	{
		if (!Application.isPlaying) return;
		SaveSystem.SaveSystem.instance.Save();
	}

	//Save with max stars to allow testing of any level
	[MenuItem("Stuart Custom Debug/ForceSaveMaxStars")]
	static void ForceSaveMaxStars()
	{
		if (!Application.isPlaying) return;
		ScoreSO[] scoreSo = Resources.FindObjectsOfTypeAll<ScoreSO>();
		scoreSo[0].IncreaseStars(99);
		SaveSystem.SaveSystem.instance.Save(new SaveSystem.SaveData());
	}

	[MenuItem("Stuart Custom Debug/ForceLoad")]
	static void ForceLoad()
	{
		if (!Application.isPlaying) return;
		LoadSystem.Load();
	}
	//Save blank and reload
	[MenuItem("Stuart Custom Debug/ClearLoadAndReload")]
	static void ClearLoadAndReload()
	{
		if (!Application.isPlaying) return;
		SaveSystem.SaveSystem.instance.ClearAndSave();
		LoadSystem.Load();
	}
}