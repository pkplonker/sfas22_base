using System.Collections;
using UnityEngine;

public class LeafParticle : MonoBehaviour
{
    void OnEnable()
    {
        StartCoroutine(SetInactiveDelay());
    }

    private IEnumerator SetInactiveDelay()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);    
    }

    
}
